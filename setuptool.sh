#!/bin/bash

python setup.py check
python setup.py register

python setup.py sdist
python setup.py bdist
python setup.py bdist_egg

python setup.py sdist upload
python setup.py bdist upload
python setup.py bdist_egg upload
