# !/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'kylinfish@126.com'
__date__ = '2015/02/14'

from test_service.conf_no_settings import *

try:
    from django.conf import settings
    from django.utils.importlib import import_module

    def project_path():
        """获取项目目录."""

        setting_module = os.environ.get("DJANGO_SETTINGS_MODULE")
        project_catalog = os.path.dirname(import_module(setting_module).__file__)
        return project_catalog

    project_path = project_path()
    service_debug = settings.DEBUG

    post_data_print = getattr(settings, "POST_DATA_PRINT", True)  # 是否输入POST数据
    post_data_saved = getattr(settings, "POST_DATA_SAVED", True)  # 是否保存请求痕迹
    exec_time_print = getattr(settings, "EXEC_TIME_PRINT", True)  # 是否输出执行时间
    save_rows_queue = getattr(settings, "SAVE_ROWS_QUEUE", 5)  # 队列满多少条保存

    login_api = getattr(settings, "LOGIN_API", None)
    logout_api = getattr(settings, "LOGOUT_API", None)

    # service API 执行时间保存文件
    # noinspection PyUnresolvedReferences
    time_report = getattr(settings, "TIME_REPORT", join(dirname(project_path), 'report', r'report_time.md'))

    # 拦截生成curl保存文件
    # noinspection PyUnresolvedReferences
    curl_report = getattr(settings, "CURL_REPORT", join(dirname(project_path), 'report', r'report_curl.md'))

    # 要查找源文件路径, 从源码文件中生成 消息码/消息文本配置信息.
    converge_list = getattr(settings, "CONVERGE_LIST", None)

except ImportError:
    raise ImportError("Django Not Installed.")
