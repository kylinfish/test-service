#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tornado.options import options
from tornado.log import app_log

options.define("post_data_print", default=True, type=bool, help="是否输入POST数据")
options.define("post_data_saved", default=True, type=bool, help="是否保存请求痕迹")
options.define("exec_time_print", default=True, type=bool, help="是否输出执行时间")
options.define("save_rows_queue", default=5, type=int, help="队列满多少条保存")

options.define("time_report", type=str, help="执行时间保存文件")
options.define("curl_report", type=str, help="拦截生成curl保存文件")

options.define("LOGIN_API", default=['/user/login/', '/user/join/'], type=list, help="登录接口")
options.define("LOGOUT_API", default=['/user/logout/'], type=list, help="注销接口")

options.define("converge_conf", default=[], type=dict, help="执行时间保存文件")
"""
    'output_file_name'      # 查找结果保存文件名.
    'is_distinct_sort'      # 是否取消重复行并排序.
    'data_format_string'    # 数据保存到文件字符串格式.
    'converge_list'         # 要查找源文件路径, 从源码文件中生成 消息码/消息文本配置信息.
"""


def parse_options_config(conf_file='tornado.conf'):
    options.parse_command_line()
    app_log.info("初始化配置文件：%s" % conf_file)
    options.options.parse_config_file(conf_file)
