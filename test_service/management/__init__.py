#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from django.core.management.base import BaseCommand
    from django.core.management.base import NoArgsCommand
except ImportError:
    BaseCommand = None
    NoArgsCommand = None
