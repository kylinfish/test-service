# !/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'kylinfish@126.com'
__date__ = '2015/02/14'

import os
import logging

logging.basicConfig()

log = logging.getLogger("test_service")
log.setLevel(logging.DEBUG)

web_frame = os.getenv("test_service")
web_frame = web_frame.lower()

if web_frame:
    if web_frame == 'django':
        try:
            from test_service.conf_be_settings import *
        except ImportError:
            pass
    elif web_frame == 'tornado':
        try:
            from test_service import options, parse_options_config
        except ImportError:
            pass
        else:
            parse_options_config()
            me_options_dict = options.as_dict()

            post_data_print = me_options_dict.get("POST_DATA_PRINT", True)  # 是否输入POST数据
            post_data_saved = me_options_dict.get("POST_DATA_SAVED", True)  # 是否保存请求痕迹
            exec_time_print = me_options_dict.get("EXEC_TIME_PRINT", True)  # 是否输出执行时间
            save_rows_queue = me_options_dict.get("SAVE_ROWS_QUEUE", 5)  # 队列满多少条保存

            login_api = me_options_dict.get("LOGIN_API", None)
            logout_api = me_options_dict.get("LOGOUT_API", None)

            # service API 执行时间保存文件
            # noinspection PyUnresolvedReferences
            time_report = me_options_dict.get("TIME_REPORT", join(dirname(project_path), 'report', r'report_time.md'))

            # 拦截生成curl保存文件
            # noinspection PyUnresolvedReferences
            curl_report = me_options_dict.get("CURL_REPORT", join(dirname(project_path), 'report', r'report_curl.md'))

            # 要查找源文件路径, 从源码文件中生成 消息码/消息文本配置信息.
            converge_list = me_options_dict.get("CONVERGE_LIST", None)
    else:
        pass
