#!usr/bin/env python
# coding: utf-8

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from test_service import tornado_request


class MainHandler(tornado.web.RequestHandler):
    @tornado_request
    def get(self):
        self.write("Hello, world")
